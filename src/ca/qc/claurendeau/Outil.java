package ca.qc.claurendeau;
/**
 * Outils en location proposés par le magasin
 * @author 03738
 *
 */
public class Outil {
	public static final int GROS_OUTILLAGE = 2;
	public static final int NOUVEAUTEES = 1;
	public static final int REGULIER = 0;
	
    private String _titre;
	private int _codePrix;

	public Outil(String titre, int codePrix) {
		_titre = titre;
		_codePrix = codePrix;
	}

	public int getCodePrix() {
		return _codePrix;
	}

	public void setCodePrix(int arg) {
		_codePrix = arg;
	}

	public String getTitre() {
		return _titre;
	}
}
